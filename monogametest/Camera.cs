﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace monogametest
{
    class Camera
    {
        public int x { get; set; }
        public int y { get; set; }
        public int max_y { get; set; }

        public Camera()
        {
            x = 0;
            y = 0;
            this.max_y = 500;
        }

        public void update_y(int y)
        {
            this.y = y;
            if (this.y > this.max_y)
            {
                this.y = this.max_y;
            }
            if (this.y < this.max_y - 50)
            {
                this.max_y = this.max_y - 50;
            }
        }
    }
}
