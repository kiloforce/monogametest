﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
#endregion

namespace monogametest
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class CowJumper : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        //int Player.player_x = 100;
        //int player_y = 100;
        //float player_rotate = 0f;
        //int player_x_dir = 1;
        //int player_y_dir = 1;
        //int player_speed = 2;
        //int player_width = 0;
        //int player_height = 0;
        //int player_half_width = 0;
        //int player_half_height = 0;

        Camera camera;
        Player Hero;

        Texture2D backgroundImg;
        Texture2D playerImg;
        SpriteFont font;

        List<Wall> walls = new List<Wall> { };

        FPS fps = new FPS();

        public CowJumper() : base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            camera = new Camera();
            Hero = new Player(camera);
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            graphics.PreferredBackBufferWidth = 800;
            graphics.PreferredBackBufferHeight = 600;
            graphics.ApplyChanges();
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            backgroundImg = Content.Load<Texture2D>("background"); // change these names to the names of your images
            playerImg = Content.Load<Texture2D>("player");

            // testfont source: https://github.com/CartBlanche/MonoGame-Samples/blob/master/TouchGesture/Content/Font.xnb
            font = Content.Load<SpriteFont>("testfont");

            //System.Console.WriteLine(playerImg.Width.ToString() + " x " + playerImg.Height.ToString());

            //Hero.width = playerImg.Width;
            //Hero.height = playerImg.Height;
            //Hero.half_width = Hero.width / 2;
            //Hero.half_height = Hero.width / 2;

            Hero.width = 10;
            Hero.height = 10;
            Hero.half_width = Hero.width / 2;
            Hero.half_height = Hero.width / 2;

            Hero.center = new Vector2(Hero.width / 2.0f, Hero.height / 2.0f);
            //player_x_center = playerImg.Width / 2;
            //player_y_center = playerImg.Height / 2;

            // TODO: use this.Content to load your game content here

            walls.Add(new Wall(camera, -100, 500, 900, 300)); // floor
            Random rand = new Random();
            int last_pos = 300;
            int new_pos;
            int new_dir;
            for (int i = 0; i < 80; i++)
            {
                new_dir = rand.Next(1, 3);
                if (new_dir == 2)
                    new_pos = last_pos + rand.Next(50, 100);
                else
                    new_pos = last_pos - rand.Next(50, 100);
                if (new_pos < 0)
                {
                    new_pos = 750;
                }
                if (new_pos > 750)
                {
                    new_pos = 0;
                }
                last_pos = new_pos;
                walls.Add(new Wall(camera, new_pos, 400 - (i * 50)));
                //walls.Add(new Wall(camera, rand.Next(last_pos - 100, last_pos + 100), 400 - (i * 50)));
            }
        }

        protected void ResetGame()
        {
            Hero.dead = false;
            Hero.won = false;
            Hero.x = 400;
            Hero.y = 450;
            Hero.velocity = 0;
            camera.max_y = 500;
            camera.x = 0;
            camera.y = 0;
            Hero.score = 0;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        protected override void Update(GameTime gameTime)
        {
            bool auto_move = true;

            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) || Keyboard.GetState().IsKeyDown(Keys.RightShift))
            {
                Hero.speed_boost();
            }
            if (Keyboard.GetState().IsKeyDown(Keys.A))
            {
                Hero.left();
            }
            if (Keyboard.GetState().IsKeyDown(Keys.D))
            {
                Hero.right();
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Space) || Keyboard.GetState().IsKeyDown(Keys.W))
            {
                Hero.jump();
            }
            else
            {
                Hero.stop_jump();
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Back))
            {
                this.ResetGame();
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Up))
            {
                auto_move = false;
                Hero.y -= 10;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Down))
            {
                auto_move = false;
                Hero.y += 10;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Left))
            {
                auto_move = false;
                Hero.x -= 10;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Right))
            {
                auto_move = false;
                Hero.x += 10;
            }
            //if (Keyboard.GetState().IsKeyDown(Keys.Q))
            //{
            //    Hero.rotate -= 0.1f;
            //    System.Console.WriteLine("Player Angle: " + Hero.rotate);
            //}
            //if (Keyboard.GetState().IsKeyDown(Keys.E))
            //{
            //    Hero.rotate += 0.1f;
            //    System.Console.WriteLine("Player Angle: " + Hero.rotate);
            //    System.Console.WriteLine(Math.PI.ToString() + " " + (2.0f * Math.PI).ToString());
            //}
            //if (Hero.rotate > 2.0f * (float)Math.PI)
            //{
            //    Hero.rotate -= 2.0f * (float)Math.PI;
            //}
            //if (Hero.rotate < -2.0f * (float)Math.PI)
            //{
            //    Hero.rotate += 2.0f * (float)Math.PI;
            //}


            //if (auto_move)
            //{
            //    Hero.x += Hero.x_dir * Hero.speed;
            //    Hero.y += Hero.y_dir * Hero.speed;
            //}

            //if (Hero.x < 0)
            //{
            //    Hero.x = 0;
            //    Hero.x_dir *= -1;
            //}
            //if (Hero.y < 0)
            //{
            //    Hero.y = 0;
            //    Hero.y_dir *= -1;
            //}

            //if (Hero.x > graphics.PreferredBackBufferWidth - Hero.width)
            //{
            //    Hero.x = graphics.PreferredBackBufferWidth - Hero.width;
            //    Hero.x_dir *= -1;
            //}
            //if (Hero.y > graphics.PreferredBackBufferHeight - Hero.height)
            //{
            //    Hero.y = graphics.PreferredBackBufferHeight - Hero.height;
            //    Hero.y_dir *= -1;
            //}

            //TimeSpan ts = gameTime.ElapsedGameTime;
            //System.Console.WriteLine(ts.ToString());


            // handle gravity
            if (auto_move)
                Hero.move();
            if (Hero.y > camera.max_y + 600)
                Hero.dead = true;
            Hero.update_score();

            foreach (Wall wall in walls)
            {
                Hero.Collide(wall.rect);
            }

            //camera.update_y(Hero.y - 500 - (int)(Hero.y * .1));
            camera.update_y(Hero.y - 400 + (int)(Hero.y * .05));
            //System.Console.WriteLine(camera.y);
            base.Update(gameTime);
        }

        private void draw_square(SpriteBatch spriteBatch, Rectangle rectangle, int lineWidth, Color color)
        {
            // source: http://stackoverflow.com/questions/13893959/how-to-draw-the-border-of-a-square
            Texture2D _pointTexture = new Texture2D(spriteBatch.GraphicsDevice, 1, 1);
            _pointTexture.SetData<Color>(new Color[] { Color.White });
            spriteBatch.Draw(_pointTexture, new Rectangle(rectangle.X, rectangle.Y, lineWidth, rectangle.Height + lineWidth), color);
            spriteBatch.Draw(_pointTexture, new Rectangle(rectangle.X, rectangle.Y, rectangle.Width + lineWidth, lineWidth), color);
            spriteBatch.Draw(_pointTexture, new Rectangle(rectangle.X + rectangle.Width, rectangle.Y, lineWidth, rectangle.Height + lineWidth), color);
            spriteBatch.Draw(_pointTexture, new Rectangle(rectangle.X, rectangle.Y + rectangle.Height, rectangle.Width + lineWidth, lineWidth), color);
        }

        // source: http://stackoverflow.com/questions/5751732/draw-rectangle-in-xna-using-spritebatch
        private void DrawRectangle(Rectangle coords, Color color)
        {
            var rect = new Texture2D(spriteBatch.GraphicsDevice, 1, 1);
            rect.SetData(new[] {color});
            spriteBatch.Draw(rect, coords, color);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {

            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            //Rectangle objectBounds = new Rectangle(Hero.x, Hero.y, Hero.width, Hero.height);
            
            spriteBatch.Begin();

            //spriteBatch.Draw(background, new Rectangle(0, 0, 800, 480), Color.White);
            //spriteBatch.Draw(backgroundImg, new Rectangle(0, 0, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight), Color.White);
            //spriteBatch.Draw(playerImg, new Vector2(player_x, player_y), Color.White);
            //spriteBatch.Draw(playerImg, new Vector2(Hero.x + Hero.half_width, Hero.y + Hero.half_height), null, Color.White, Hero.rotate, Hero.center, 1.0f, SpriteEffects.None, 0.0f);
            //DrawRectangle(new Rectangle(10, 10, 50, 50), Color.Aqua);
            //spriteBatch.Draw(objectBounds, null, Color.Red);
            //spriteBatch.Draw(player, new Rectangle(player_x, player_y, 50, 50), Color.White);
            //draw_square(spriteBatch, objectBounds, 2, Color.Red);
            //monogametest.PrimiviteDrawing.DrawRectangle(spriteBatch, objectBounds, 2, Color.Red);

            //monogametest.PrimiviteDrawing.DrawCircle(spriteBatch, new Vector2(100, 100), 200, Color.Red, 2);

            /* Vector2[] pvertex = new Vector2[4];
            pvertex[0] = new Vector2(100, 100);
            pvertex[1] = new Vector2(200, 100);
            pvertex[2] = new Vector2(200, 200);
            pvertex[3] = new Vector2(300, 400);
             * */
            //List<Vector2> pvertex = new List<Vector2> { };
            //pvertex.Add(new Vector2(100, 100));
            //pvertex.Add(new Vector2(200, 100));
            //pvertex.Add(new Vector2(200, 200));
            //pvertex.Add(new Vector2(300, 400));
            //Vector2[] pvertex2 = pvertex.ToArray();
            

            //PrimiviteDrawing.DrawPolygon(spriteBatch, pvertex2, Color.Red, 2);

            //Primitives2D.DrawCircle(spriteBatch, new Vector2(200, 100), 50, 32, Color.Red, 10);
            //Primitives2D.DrawCircle(spriteBatch, new Vector2(200, 100), 50, 10, Color.Red, 50);

            //Primitives2D.DrawLine(spriteBatch, new Vector2(0, 500), new Vector2(800, 500), Color.Blue);
            Primitives2D.DrawRectangle(spriteBatch, Hero.camera_rect, Color.Red);

            foreach (Wall wall in walls)
            {
                Primitives2D.DrawRectangle(spriteBatch, wall.camera_rect, Color.Green);
            }

            if (Hero.dead)
                spriteBatch.DrawString(font, "You Died!", new Vector2(350, 300), Color.Red);

            spriteBatch.DrawString(font, "Score: " + Hero.score, new Vector2(650, 10), Color.Black);

            if (Hero.won)
                spriteBatch.DrawString(font, "You Won!", new Vector2(350, 300), Color.Blue);

            // FPS
            spriteBatch.DrawString(font, "FPS: " + fps.GetFPS().ToString("0.##"), new Vector2(10, 10), Color.Black);

            spriteBatch.End();

            base.Draw(gameTime);
            fps.Update();
        }
    }
}
