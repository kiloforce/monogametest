﻿using System;


namespace monogametest
{
    public class FPS
    {

        float frameRate = 0f;
        float frameCounter = 0f;
        long lastTime = 0;
        long currentTime = 0;
        //TimeSpan elapsedTime = TimeSpan.Zero;
        //long milliseconds = 0;

        public void Update()
        {
            frameCounter += 1;
            currentTime = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
            if (currentTime > lastTime + 1000)
            {
                float diffTime = (currentTime - lastTime) / 1000.0f;
                frameRate = frameCounter / diffTime;
                //System.Console.WriteLine("new frame: " + frameCounter + " "+ frameRate + " " + diffTime);
                lastTime = currentTime;
                frameCounter = 0;
            }
        }

        public float GetFPS()
        {
            return frameRate;
        }

    }
}
