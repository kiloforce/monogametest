﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace monogametest
{
    class Player
    {
        //const int JUMP_VELOCITY = 10;
        const int JUMP_VELOCITY = 10;
        const float GRAVITY = .5f;
        public int x { get; set; }
        public int y { get; set; }
        public float rotate { get; set; }
        public int x_dir { get; set; }
        public int y_dir { get; set; }
        public int speed { get; set; } 
        public int width { get; set; } 
        public int height { get; set; }
        public int half_width { get; set; }
        public int half_height { get; set; }
        public Vector2 center { get; set; }
        public bool on_floor { get; set; }
        public float velocity { get; set; }
        public bool go_fast { get; set; }
        public Camera camera { get; set; }
        public bool dead { get; set; }
        public bool won { get; set; }
        public int score { get; set; }

        public Rectangle rect
        {
            get
            {
                return new Rectangle(x, y, width, height);
            }
        }

        public Rectangle camera_rect
        {
            get
            {
                return new Rectangle(x, y - camera.y, width, height);
            }
        }

        public Player(Camera camera)
        {
            x = 400;
            y = 450;
            rotate = 0f;
            x_dir = 1;
            y_dir = 1;
            speed = 2;
            width = 0;
            height = 0;
            half_width = 0;
            half_height = 0;
            half_width = 0;
            half_height = 0;
            on_floor = false;
            velocity = 0;
            go_fast = false;
            this.camera = camera;
            this.dead = false;
            this.won = false;
            this.score = 0;
        }

        public void jump()
        {
            // initiate jump
            if (dead || won) return;
            if (on_floor)
            {
                velocity = JUMP_VELOCITY;
            }
        }

        public void stop_jump()
        {
            if (dead || won) return;
            if (velocity > 0)
            {
                velocity = 0;
            }
        }

        public void left()
        {
            if (dead || won) return;
            this.x -= this.speed;
            if (go_fast)
            {
                this.x -= this.speed;
                go_fast = false;
            }
            if (this.x <= 0)
            {
                this.x += 800;
            }
        }

        public void right()
        {
            if (dead || won) return;
            this.x += this.speed;
            if (go_fast)
            {
                this.x += this.speed;
                go_fast = false;
            }
            if (this.x >= 800)
            {
                this.x -= 800;
            }
        }

        public void move()
        {
            if (dead || won) return;
            // handle gravity and inertia
            if (!on_floor)
            {
                y += (int)-velocity;
                velocity -= GRAVITY;
            }

            if (y > 600 - height)
            {
                velocity = 0;
                y = 600 - height;
                on_floor = true;
            }
            else
            {
                on_floor = false;
            }
        }

        public void speed_boost()
        {
            go_fast = true;
        }

        public bool Collide(Rectangle other_rect)
        {
            if(rect.Intersects(other_rect))
            {
                //System.Console.WriteLine(other_rect.X + " " + other_rect.Y);
                if (velocity < 0)
                {
                    y = other_rect.Y - height + 1;
                    on_floor = true;
                    velocity = 0;
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public void update_score()
        {
            int new_score = this.y;
            if (new_score > 0) new_score = 0;
            new_score = Math.Abs(new_score);
            if (new_score > this.score) this.score = new_score;
            if (this.score > 3560) this.won = true;

        }


        
    }
}
