﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace monogametest
{
    // Source: http://stackoverflow.com/questions/13893959/how-to-draw-the-border-of-a-square
    static public class PrimiviteDrawing
    {

        static public Texture2D CreateWhitePixel(SpriteBatch spritebatch)
        {
            Texture2D whitePixel = new Texture2D(spritebatch.GraphicsDevice, 1, 1);
            whitePixel.SetData<Color>(new Color[] { Color.White });
            return whitePixel;
        }

        // DrawRectangle
         static public void DrawRectangle(SpriteBatch spritebatch, Rectangle area, int width, Color color)
        {
            Texture2D whitePixel = CreateWhitePixel(spritebatch);
            DrawRectangle(whitePixel, spritebatch, area, width, color);
        }

        static public void DrawRectangle(Texture2D whitePixel, SpriteBatch batch, Rectangle area, int width, Color color)
        {

            batch.Draw(whitePixel, new Rectangle(area.X, area.Y, area.Width, width), color);
            batch.Draw(whitePixel, new Rectangle(area.X, area.Y, width, area.Height), color);
            batch.Draw(whitePixel, new Rectangle(area.X + area.Width - width, area.Y, width, area.Height), color);
            batch.Draw(whitePixel, new Rectangle(area.X, area.Y + area.Height - width, area.Width, width), color);
        }

        // DrawCircle
        public static void DrawCircle(SpriteBatch spritebatch, Vector2 center, float radius, Color color, int lineWidth = 2, int segments = 16)
        {
            Texture2D whitePixel = CreateWhitePixel(spritebatch);
            DrawCircle(whitePixel, spritebatch, center, radius, color, lineWidth, segments);
        }

        public static void DrawCircle(Texture2D whitePixel, SpriteBatch spritebatch, Vector2 center, float radius, Color color, int lineWidth = 2, int segments = 16)
        {
            Vector2[] vertex = new Vector2[segments];

            double increment = Math.PI * 2.0 / segments;
            double theta = 0.0;

            for (int i = 0; i < segments; i++)
            {
                //vertex[i] = center.ToVector2() + radius * new Vector2((float)Math.Cos(theta), (float)Math.Sin(theta));
                vertex[i] = center + radius * new Vector2((float)Math.Cos(theta), (float)Math.Sin(theta));
                theta += increment;
            }

            DrawPolygon(whitePixel, spritebatch, vertex, color, lineWidth);
        }

        // DrawPolygon
        public static void DrawPolygon(SpriteBatch spritebatch, Vector2[] vertex, Color color, int lineWidth)
        {
            Texture2D whitePixel = CreateWhitePixel(spritebatch);
            DrawPolygon(whitePixel, spritebatch, vertex, color, lineWidth);
        }

        public static void DrawPolygon(Texture2D whitePixel, SpriteBatch spriteBatch, Vector2[] vertex, Color color, int lineWidth)
        {
            int count = vertex.Length;
            if (count > 0)
            {
                for (int i = 0; i < count - 1; i++)
                {
                    DrawLineSegment(whitePixel, spriteBatch, vertex[i], vertex[i + 1], color, lineWidth);
                }
                DrawLineSegment(whitePixel, spriteBatch, vertex[count - 1], vertex[0], color, lineWidth);
            }
        }

        // DrawLineSegment
        public static void DrawLineSegment(SpriteBatch spritebatch, Vector2 point1, Vector2 point2, Color color, int lineWidth)
        {
            Texture2D whitePixel = CreateWhitePixel(spritebatch);
            DrawLineSegment(whitePixel, spritebatch, point1, point2, color, lineWidth);
        }

        public static void DrawLineSegment(Texture2D whitePixel, SpriteBatch spriteBatch, Vector2 point1, Vector2 point2, Color color, int lineWidth)
        {
            float angle = (float)Math.Atan2(point2.Y - point1.Y, point2.X - point1.X);
            float length = Vector2.Distance(point1, point2);

            spriteBatch.Draw(whitePixel, point1, null, color,
            angle, Vector2.Zero, new Vector2(length, lineWidth),
            SpriteEffects.None, 0f);
        }

    }
}
