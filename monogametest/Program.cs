﻿using System;

namespace monogametest
{
    public static class Program
    {
        static void Main()
        {
            System.Console.WriteLine("Game starting...");
            CowJumper game = new CowJumper();
            game.Run();
            System.Console.WriteLine("Game ending...");
        }
    }
}
