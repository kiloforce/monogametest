﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace monogametest
{
    class Wall
    {
        public int x { get; set; }
        public int y { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public Camera camera { get; set; }
        public Rectangle rect
        {
            get
            {
                return new Rectangle(x, y, width, height);
            }
        }

        public Rectangle camera_rect
        {
            get
            {
                return new Rectangle(x, y - camera.y, width, height);
            }
        }

        public Wall(Camera camera, int x, int y, int width = 50, int height = 10)
        {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
            this.camera = camera;
        }


    }
}
